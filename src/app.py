from flask import Flask
from flask import request
app = Flask(__name__)

employees = ['Bill Gates', 'Steve Jobs', 'Richard Stallman']

# create html for an example employee list
def render_list(list):
	html = 'Employees:<ul>'
	for item in list:
		html += '<li>{0}</li>'.format(item)
	html += '</ul>'
	return html

@app.route('/')
def hello_world():
	# grab given username and password for http query
	username = request.args.get('user')
	password = request.args.get('password')

	if not username or not password:
		return 'Unauthenticated login not supported'

	# log that someone is trying to authenticate
	app.logger.info('Login attempt made for {0}'.format(username))

	# check the password they provided is correct
	# obviously hardcoded, unhashed password and plaintext login with query param
	# is not secure at all, but this is just for demonstration of CRLF
	if username == 'admin' and password == 'abc':
		app.logger.info('{0} successfully logged in'.format(username))
		return render_list(employees)

	# if the username/password is incorrect, deny the request
	return 'Incorrect user name/password'
	
if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')