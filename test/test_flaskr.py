import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'src'))

import pytest
import logging

from app import app

@pytest.fixture
def client():
    with app.test_client() as client:
        yield client

def test_unauthenticated_login(client):
	rv = client.get('/')
	assert b'Unauthenticated login not supported' in rv.data

def test_missing_password(client):
	rv = client.get('/?user=admin')
	assert b'Unauthenticated login not supported' in rv.data

def test_missing_username(client):
	rv = client.get('/?password=abc')
	assert b'Unauthenticated login not supported' in rv.data

def test_incorrect_username(client):
	rv = client.get('/?user=root&password=abc')
	assert b'Incorrect user name/password' in rv.data

def test_incorrect_password(client):
	rv = client.get('/?user=admin&password=123')
	assert b'Incorrect user name/password' in rv.data

def test_authenticated_login(client, caplog):
	caplog.set_level(logging.INFO)
	# give correct login credentials
	rv = client.get('/?user=admin&password=abc')
	# check the attempt was logged
	assert caplog.records[0].msg == 'Login attempt made for admin'
	# checked the success was logged
	assert caplog.records[1].msg == 'admin successfully logged in'
	# check website gave an employee list
	assert b'Employees:' in rv.data

def test_crlf(client, caplog):
	caplog.set_level(logging.INFO)
	# give fake login details, and inject a fake successful login log using CRLF
	rv = client.get('/?password=guess&user=admin%0d%0aadmin%20successfully%20logged%20in')
	# check the fake successful login log does not make it onto a new line
	assert caplog.records[0].msg == 'Login attempt made for adminadmin successfully logged in'